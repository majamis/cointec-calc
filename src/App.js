import React, { Component } from 'react'
import ReduxThunk from 'redux-thunk'
import { Provider } from "react-redux"
import { createStore, applyMiddleware } from "redux"
import reducers from "./Redux/reducers"
import promise from 'redux-promise'
import Calculator from './Components/Calculator'
import SimpleCalculator from './Components/SimpleCalculator'

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

class App extends Component {
  render() {
    return (
      <Provider store={createStoreWithMiddleware(reducers, {}, applyMiddleware(ReduxThunk))}>
        <div>
          <Calculator />
         {/* <SimpleCalculator /> */}
        </div>
      </Provider>
    );
  }
}

export default App;
