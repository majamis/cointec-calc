import React, { Component } from 'react'
import { formValueSelector, reduxForm } from 'redux-form'
import cn from 'classnames'
import './style.css'
import { createOrder, clearOrder, abandonOrder, getStatus } from '../../../../Redux/actions'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import Loader from '../ProcessingOrder'
import CompleteOrder from '../CompleteOrder';

class CreateOrderForm extends Component {
    constructor() {
        super()
        this.state = { addAccountModal: false }
        this.handleClick = this.handleClick.bind(this)
        this.renderButton = this.renderButton.bind(this)
        this.rateExpired = this.rateExpired.bind(this)
        this.tick = this.tick.bind(this)
        this.handleContinue = this.handleContinue.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.renderScreen = this.renderScreen.bind(this)
        this.handleRemove = this.handleRemove.bind(this)

        this.state = {
            timerId: null,
            timer: 0,
            refreshTime: 10,
            status: null,
            pollTime: 30,
        }
    }

    tick() {
        if(this.state.timer % this.state.pollTime === 0)
        {
            this.props.getStatus(this.props.order.create.CtTransactionId)
        }
        this.setState({
          timer: this.state.timer + 1
        });
        console.log(this.state.timer)
    }

    componentWillMount() {
        this.props.createOrder({
            destAmount: this.props.btc, 
            sourceAmount: this.props.gbp,
            exchangeRate: this.props.rate,
            dest: this.props.wallet
        })
    }

    handleClick(e) {
        this.props.callback()
    }

    handleRemove() {
        this.props.abandonOrder(this.props.order.create.CtTransactionId)
        this.props.callback()
    }

    componentWillUnmount() {
        clearInterval(this.state.timerId);
    }

    rateExpired() {
        // reset timer
        this.setState({ timer: 0 })
    }

    handleContinue() {
        this.props.clearOrder(this.props.order.create.CtTransactionId)
        let timerId = setInterval(this.tick, 1000);
        this.setState( { status: 'clear', timerId });
    }
    
    verifyCallback = (response) => {
        console.log(response);
    }
    
    renderButton() {
        //if(this.state.timer > this.state.refreshTime)
        //    return <button onClick={this.rateExpired} className={cn('btn-block btn-lg', 'btn-danger')}>Rate expired - click to refresh</button>
        //else
            return <button onClick={this.handleContinue} type="submit" className={cn('btn-block btn-lg', 'btn-primary')}>I have made the bank transfer</button>
    }

    onSubmit(values) {
        
    }

    renderScreen() {
        const { rate, gbp, btc, handleSubmit } = this.props;
        const { owner, sortCode, accountName, accountNumber } = this.props.order.create.BrokerAccount;
        let titles = {
            0:  {
                'title': 'Beneficary',
                'value': `${owner}`,
            },
            1:  {
                'title': 'Account number',
                'value': `${accountNumber}`
            },
            2: {
                'title': 'Sort code',
                'value': `${sortCode}`
            },
            3: {
                'title': 'Amount',
                'value': `£${gbp}`
            },
            4: {
                'title': 'Reference',
                'value': `{22444838}`
            }
        }
        let titleList = Object.values(titles).map(function(key,index) {
            return (
                <div key={index} className="row">
                    <div className="col-md-5">
                        <h5>{key.title}</h5>
                    </div>
                    <div className="col-md-7">
                        <h5>{key.value}</h5>
                    </div>
                </div>
            )
        })
        
        return(
            <div className="wrapper-320">
                <form onSubmit={handleSubmit(this.onSubmit)}>
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-md-12">
                                <span onClick = {this.handleClick}className="back glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                                <h4 className="inline-headers">Pay by bank transfer</h4>
                                <span onClick = {this.handleRemove}className="remove glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </div>
                        </div>
                        <hr />
                        { titleList }
                        <div className = "row">
                            <div className = "col-md-12">
                                <p className="info">Your coins are sent when we receieve payment</p>
                            </div>
                            <div className = "col-md-12">
                                <label className="wallet-label">{this.props.wallet}</label>
                            </div>
                        </div>
                        <div className = "row bt-margin">
                            <div className = "col-md-12">
                            {
                                this.renderButton()
                            }
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }

    render() {
        const { loading } = this.props.order;
        console.log(this.props.order)
        if(this.props.order.create === null || loading) {
            return (
               <Loader />
            )
        }
        else if(this.state.status === 'clear') {
            if(this.props.order.status === null) {
                return (
                    <Loader />
                )
            }
            else if(this.props.order.status.Status === 'EXECUTED') {
                return <CompleteOrder txnID={this.props.order.status.LedgerTxnId} />
            }
            else {
                return <Loader />
            }
        }
        else
            return this.renderScreen()

    }
    componentWillReceiveProps(props) {
    
    }
    
}

const mapStateToProps = (state) => {
    const selector = formValueSelector('CreateOrderForm')
    return { order: state.order };
}

export default reduxForm({
    form: 'CreateOrderForm',
})(connect(mapStateToProps,{createOrder,clearOrder, abandonOrder, getStatus})(CreateOrderForm));;

CreateOrderForm.propTypes = {
    btc: PropTypes.number,
    rate: PropTypes.number,
    gbp: PropTypes.number,
    wallet: PropTypes.string,
    callback: PropTypes.func
}