import React, { Component } from 'react'
import { formValueSelector, Field, reduxForm } from 'redux-form'
import cn from 'classnames'
import './style.css'
import AddBankAccount from '../AddBankAccount'
import { fetchQuote } from '../../../../Redux/actions'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import Modal from '../Modal/';
import Recaptcha from 'react-grecaptcha';

class ReviewForm extends Component {
    constructor() {
        super()
        this.state = { addAccountModal: false }
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.renderButton = this.renderButton.bind(this)
        this.rateExpired = this.rateExpired.bind(this)
        this.tick = this.tick.bind(this)
        this.handleContinue = this.handleContinue.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

        this.state = {
            timerId: null,
            timer: 0,
            refreshTime: null,
            selectLast: false,
            showCaptcha: false
        }
    }

    tick() {
        this.setState({
          timer: this.state.timer + 1
        });
        console.log(this.state.timer)
    }

    componentWillMount() {
        let timerId = setInterval(this.tick, 1000);
        this.setState({ timerId, refreshTime: this.props.refreshTime });
    }

    handleClick(e) {
        this.props.callback()
    }

    closeModal(accountAdded = false) {
        this.setState({addAccountModal: false, selectLast: accountAdded });
    }

    handleChange(event) {
        if(event.target.value === 'addBank') {
            this.setState({ addAccountModal: true });
        }
    }

    componentWillUnmount() {
        clearInterval(this.state.timerId);
    }

    rateExpired() {
        // reset timer
        this.props.fetchQuote({ SendAmount: this.props.gbp })
        this.setState({ timer: 0 })
    }

    handleContinue() {
        this.setState({ showCaptcha : true })
    }
    
    verifyCallback = (response) => {
        //console.log(response);
        this.props.callback('third');
    };
    
    expiredCallback = () => {
        console.log(`Recaptcha expired`);
    }
    
    renderButton() {
        if(this.state.showCaptcha) {
            return (
                <Recaptcha
                    sitekey={'6Ld5nFUUAAAAANRvB37_utUYF0-keXqw_i105cGm'}
                    callback={this.verifyCallback}
                    expiredCallback={this.expiredCallback}
                    locale="en"
                    size="compact"
                    data-theme="light"
                />
            )
        }
        else if(this.state.timer > this.state.refreshTime)
            return <button onClick={this.rateExpired} className={cn('btn-block btn-lg', 'btn-danger')}>Rate expired - click to refresh</button>
        else
            return <button onClick={this.handleContinue} type="submit" className={cn('btn-block btn-lg', 'btn-primary')}>Continue</button>
    }

    onSubmit(values) {
        
    }

    render() {
        const { accounts, rate, gbp, btc, handleSubmit } = this.props;
        let accountList = null

        if(accounts) {
            accountList = accounts.map((value,index) => {
                return <option value={index} key={index}>{value.SortCode} - {value.AccountReference}  </option>
            });
        }
       
        let titles = {
            0:  {
                'title': 'You Receieve',
                'value': `${btc} BTC`,
            },
            1:  {
                'title': 'Exchange Rate',
                'value': `${rate}/BTC`
            },
            2: {
                'title': 'You Pay',
                'value': `£${gbp}`
            }
        }
        let titleList = Object.values(titles).map(function(key,index) {
            return (
                <div key={index} className="row">
                    <div className="col-md-5">
                        <h5>{key.title}</h5>
                    </div>
                    <div className="col-md-7">
                        <h5>{key.value}</h5>
                    </div>
                </div>
            )
        })
        
        return(
            <div className="wrapper-320">
                <form onSubmit={handleSubmit(this.onSubmit)}>
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-md-12">
                                <span onClick = {this.handleClick}className="back glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                                <h4 className="inline-headers">Review and confirm order</h4>
                            </div>
                        </div>
                        <hr />
                        { titleList }
                        
                        <div className = "row flex-center">
                                <div className="col-md-5">
                                    <h5>Send from</h5>
                                </div>
                                <div className="col-md-7">
                                    <Field name="sendFrom" component="select" className="form-control" onChange={this.handleChange}>
                                        { accountList }
                                        <option value="addBank">Add a new bank</option>
                                    </Field>
                                </div>
                        </div>
                        <div className = "row">
                            <div className = "col-md-12">
                                <h5>External wallet address</h5>
                            </div>
                            <div className = "col-md-12">
                                <label className="wallet-label">{this.props.wallet}</label>
                            </div>
                        </div>
                        <div className = "row bt-margin">
                            <div className = "col-md-12">
                            {
                                this.renderButton()
                            }
                            </div>
                        </div>
                    </div>
                </form>
                <Modal
                    show={this.state.addAccountModal}
                    onClose={this.closeModal}
                    
                >
                    <AddBankAccount close={this.closeModal}/>
                </Modal>
            </div>
        )
    }
    componentWillReceiveProps(props) {
        if(props.sendFrom === 'addBank')
            this.props.change('sendFrom',null)
        if(this.state.selectLast && props.bank.fetched) {
            this.setState({selectLast: false})
            props.change('sendFrom',props.accounts.length-1)
        }
    }
    
}

const mapStateToProps = (state) => {
    const selector = formValueSelector('ReviewForm')
    let sendFrom = (selector(state, 'sendFrom'))
    return { bank: state.bank, sendFrom };
}

export default reduxForm({
    form: 'ReviewForm',
})(connect(mapStateToProps,{ fetchQuote })(ReviewForm));;

ReviewForm.propTypes = {
    btc: PropTypes.number,
    rate: PropTypes.number,
    gbp: PropTypes.number,
    refreshTime: PropTypes.number,
    wallet: PropTypes.string,
    accounts: PropTypes.array,
    callback: PropTypes.func
}