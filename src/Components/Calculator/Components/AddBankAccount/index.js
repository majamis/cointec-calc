import React,{ Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { addAccount } from '../../../../Redux/actions/index'
import cn from 'classnames'

import './style.css'

const errorMap = {
    default: 'Bank must Faster Payments enabled',
    400: 'Please check your account details.',
    406: 'Account is not Faster-Payments enabled.',
    409: 'You have already added this account.' 
}

class AddBankAccount extends Component {

    constructor() {
        super()
        this.onSubmit = this.onSubmit.bind(this)
        this.state = { 
            loading: false,
            error: {
                text: null,
                status: null
            }
        }
        this.handleClick = this.handleClick.bind(this)
    }

    componentWillReceiveProps(props) {
        if(props.bank) {
            this.setState({ loading: props.bank.loading })

            if(props.bank.error) {
                const { status } = props.bank.error.response
                this.setState({ error: { text: errorMap[status], status} })
            }
            else
                this.setState({ error: { text: errorMap.default, status: null } })
            
            if(props.bank.addFN && props.bank.addFN.Success)
            {
                this.props.close(true)
            }
        }
    }

    renderButton() {
        let buttonState = ''
        let buttonClass = 'btn-success'
        if(this.state.loading) {
            buttonState = 'disabled'
            buttonClass = ''
        }
        
        return <button type="submit" className={cn('btn-block btn-lg',buttonClass,buttonState)} disabled={buttonState}>Add Bank Account</button>
    }

    renderField(field) {
        const { placeholder, valid , meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-warning' : '' } ${valid === true ? 'has-success' : '' }  ${valid === false ? 'has-warning' : '' }`;
        return (
            <div className={className}>
                <input placeholder={placeholder} className="form-control"
                    {...field.input}
                />
                <div className = "text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    normalizeSortCode(value) {
        if (!value) {
          return value
        }
      
        const onlyNums = value.replace(/[^\d]/g, '')
        if (onlyNums.length <= 2) {
          return onlyNums
        }
        if (onlyNums.length <= 4) {
          return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2)}`
        } 
        return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2, 4)}-${onlyNums.slice(
            4,
            6
        )}`
    }

    normalizeAccountNumber(value) {
        if (!value) {
          return value
        }
        const onlyNums = value.replace(/[^\d]/g, '')

        return onlyNums.slice(0, 8)
    }

    onSubmit(values) {
        this.props.addAccount(5,values)
    }

    handleClick() {
        this.props.close()
    }

    render() {
        const { handleSubmit } = this.props
        return (
            <form onSubmit={handleSubmit(this.onSubmit)}>
                <div className="pd-20">
                    <div className="row">
                        <div className="col-md-12">
                            <h4 className="inline-headers-close">Add a new bank account</h4>
                            <span onClick = {this.handleClick}className="close glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </div>
                    </div>
                    <hr />
                    <div className="row buffer-top">
                        <div className="col-md-12">
                            <label>Account Name</label>
                            <Field
                                name="accountName"
                                component={this.renderField}
                                placeholder={'Account 1'}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <label>Sort Code</label>
                            <Field
                                name="sortCode"
                                normalize={this.normalizeSortCode}
                                component={this.renderField}
                                placeholder='XX-XX-XX'
                            />
                        </div>
                        <div className="col-md-6">
                            <label>Account No.</label>
                            <Field
                                name="accountNo"
                                normalize={this.normalizeAccountNumber}
                                component={this.renderField}
                                placeholder='XXXXXXXX'
                            />
                        </div>
                    </div>
                    <div className = "row">
                        <div className = "col-md-12">
                            <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <p className="inline-headers info">{this.state.error.text}</p>
                        </div>
                    </div>
                    <div className = "row bt-margin">
                        <div className = "col-md-12">
                            { this.renderButton() }
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

function validate(values, props) {
    const errors = {};
    // validate inputs from 'values'
    if(!values.accountName) {
      errors.accountName = "Enter Account Name";
    }

    if (!values.sortCode) {
      errors.sortCode = "Enter Sort Code";
    }
    
    if(!values.accountNo) {
      errors.accountNo = "Enter Account No";
    }
  
    return errors;
}

 
const mapStateToProps = (state) => {
    return { bank: state.bank };
}

export default reduxForm({
    form: 'AddBankForm',
    validate
})(connect(mapStateToProps,{ addAccount })(AddBankAccount));