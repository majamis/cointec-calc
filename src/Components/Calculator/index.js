import React, { Component } from 'react'
import { formValueSelector, Field, reduxForm } from 'redux-form'
import { fetchQuote, fetchLimit, fetchConsts, fetchAccounts } from '../../Redux/actions/index'
import { connect } from 'react-redux'
import './style.css'
import cn from 'classnames'
import walletValidator from 'wallet-address-validator'
import ReviewForm from './Components/ReviewForm'
import CreateOrderForm from './Components/CreateOrderForm'
import _ from 'lodash'

class Calculator extends Component {
    
    constructor() {
        super()
        this.state = {
            placeholder: 250,
            placeholderBTC: null,
            rate: 1200,
            currencySymbol: '£',
            limit: 0,
            limitMin: 15,
            buttonIsDisabled: false,
            screen: 'first',
            active: 'gbp',
            intervalId: null,
            interval: 60,
            reviewRefreshTime: 60,
            debouncedBTC: null,
            debouncedGBP: null,
        }
        this.fistScreen = this.fistScreen.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.updateLimit = this.updateLimit.bind(this)
        this.updateRate = this.updateRate.bind(this)
        this.updateButtonState = this.updateButtonState.bind(this)

        this.normalizeGBP = this.normalizeGBP.bind(this)
        this.normalizeBTC = this.normalizeBTC.bind(this)
        this.convertToBTC = this.convertToBTC.bind(this)
        this.convertToGBP = this.convertToGBP.bind(this)
        this.fetchCalls = this.fetchCalls.bind(this)
        this.limitGBP = this.limitGBP.bind(this)
        this.back = this.back.bind(this)
    }

    back(screen) {
        if(screen !== 'third') {
            let intervalId = setInterval(this.fetchCalls, this.state.interval * 1000);
            this.setState({screen: 'first', intervalId})
        }
        else
        {
            this.setState({screen})
        }
    }

    normalizeGBP(value, previousValue) {
        let decimalPoint = 2
        let gbp = value.replace(/[^\d.]/g, '')
        let pos = gbp.indexOf('.')

        this.setState({active: 'gbp'})
        if(pos >= 0) {
            // prevent an extra decimal point
            if(gbp.indexOf('.',pos+1) > 0)
                gbp = gbp.substring(0,pos+1)
            // allow up to 2 dp
            if(gbp.length - pos > 2 )
                gbp = gbp.substring(0,(pos+1)+decimalPoint)
        }

        if(value !== previousValue)
        {
            if(gbp.length > 0) {
                if(this.state.debouncedGBP === null) {
                    this.setState( { debouncedGBP: _.debounce((gbp) => { console.log('gbp',gbp), this.props.fetchQuote({'SendAmount': Number.parseFloat(gbp)})}, 500,  { 'trailing': true }) }, () => { 
                        this.state.debouncedGBP(gbp)
                    })
                }

                if(this.state.debouncedGBP) {
                    this.state.debouncedGBP(gbp);
                }
            }
            else
            {
                //this.props.fetchQuote({'ReceiveAmount': Number.parseFloat(this.state.placeholder)})
                this.props.change('btc', null)
            }
        }

        if(gbp.length > 0)
            return '£ ' + gbp
        else
            return gbp
    }
    
    normalizeBTC(value, previousValue) {
        let decimalPoint = 8
        let btc = value.replace(/[^\d.]/g, '')
        let pos = btc.indexOf('.')
        
        if(pos >= 0) {
            // prevent an extra decimal point
            if(btc.indexOf('.',pos+1) > 0)
                btc = btc.substring(0,pos+1)
            // allow up to 2 dp
            if(btc.length - pos > 2 )
                btc = btc.substring(0,(pos+1)+decimalPoint)
        }
        
        if(value !== previousValue)
        {
            if(btc.length > 0) {
                this.setState({active: 'btc'})
                if(this.state.debouncedBTC === null)
                    this.setState( { debouncedBTC: _.debounce((btc) => { console.log('btc',btc), this.props.fetchQuote({'ReceiveAmount': Number.parseFloat(btc)})}, 500,  { 'trailing': true }) }, () => {
                        this.state.debouncedBTC(btc) 
                    })
                
                if(this.state.debouncedBTC)
                    this.state.debouncedBTC(btc);
            }
            else {
                console.log('here')
                // fetch btc to reset default rate
                this.props.fetchQuote({'SendAmount': Number.parseFloat(this.state.placeholder)})
                this.props.change('gbp', null)
                this.setState({active: 'gbp'})
            }
        }

        return btc
    }
      

    initInterval(interval) {
        clearInterval(this.state.intervalId)
        let intervalId = setInterval(this.fetchCalls, interval * 1000);
        // store intervalId in the state so it can be accessed later to clear it
        this.setState({intervalId: intervalId})
    }

    componentDidMount() {
        // set call fetch interval 
        this.initInterval(this.state.interval)
        // fetch call the first time component mounts
        this.fetchCalls()
        this.props.fetchAccounts(5)
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId)
    }

    fetchCalls() {
        this.props.fetchLimit();
        this.props.fetchConsts();
        if(this.state.active === 'gbp') {
            this.props.fetchQuote({'SendAmount': this.props.gbp ? this.props.gbp : this.state.placeholder});
        }
        else if(this.state.active === 'btc' && this.props.btc) {
            this.props.fetchQuote({'ReceiveAmount': this.props.btc });
        }
    }
    

    componentWillReceiveProps(props) {
        console.log(props);
        if(props.quote.SendAmount === this.state.placeholder)
            this.setState({ placeholderBTC: props.quote.ReceiveAmount })
        if(props.gbp && this.state.active === 'gbp' && props.quote.ReceiveAmount)
            this.props.change('btc',Number.parseFloat(props.quote.ReceiveAmount).toFixed(8))
            
        if(props.btc && this.state.active === 'btc' && props.quote.SendAmount)
            this.props.change('gbp',this.state.currencySymbol + ' ' + Number.parseFloat(props.quote.SendAmount).toFixed(2))
            
        this.updateLimit(props)
        this.updateRate(props)
        this.updateButtonState(props)
    }

    onSubmit(values) {
        this.setState({ screen: 'second'} )
        clearInterval(this.state.intervalId)
    }

    convertToBTC(amount) {
        return amount / this.state.rate
    }

    convertToGBP(amount) {
        return amount * this.state.rate
    }

    renderField(field) {
        const { placeholder, valid , meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-warning' : '' } ${valid === true ? 'has-success' : '' }  ${valid === false ? 'has-warning' : '' }`;
        return (
            <div className={className}>
                <input placeholder={placeholder} className="form-control"
                    {...field.input}
                />
                <div className = "text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    updateRate(props) {
        if(props.quote.ExchangeRate) {
            this.setState({ rate: Number.parseFloat(props.quote.ExchangeRate) })
        }
    }

    updateLimit(props) {
        
        if (props.limit.limit) {
            this.setState({ limit: props.limit.limit })
        }
        if (props.limit.const) {
            let interval = props.limit.const.Frame1Refresh
            let refreshTime = props.limit.const.Frame2Refresh
            if(this.state.interval != interval) {
                this.initInterval(interval)
                this.setState({ interval })
            }
            if(this.state.reviewRefreshTime != refreshTime) {
                this.setState({ reviewRefreshTime: refreshTime })
            }
        }
    }

    updateButtonState(props) {
        if(this.state.limit < props.gbp || this.state.limitMin > props.gbp || (!props.validWallet && props.wallet))
        {
            this.setState({buttonIsDisabled: true})
        }
        else
            this.setState({buttonIsDisabled: false})
    }

    renderButton() {
        let buttonState = ''
        let buttonClass = 'btn-success'
        if(this.state.buttonIsDisabled ) {
            buttonState = 'disabled'
            buttonClass = ''
        }
        return <button type="submit" className={cn('btn-block btn-lg',buttonClass,buttonState)} disabled={buttonState}>Instant exchange</button>
    }
    
    limitGBP(value) {
        value = '"' + value
        value = Number.parseFloat(value.split(' ')[1])
        return (value < this.state.limitMin || value > this.state.limit)? `Must be between ${this.state.limitMin} - ${this.state.limit}` : undefined
    }

    fistScreen() {
        const { handleSubmit } = this.props
        return (
            <form onSubmit={handleSubmit(this.onSubmit)}>
                <div className="wrapper-320 wrapper">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row">
                                <h5 className="text-center">Deposit</h5>
                            </div>
                            <div className="row">
                                <h2 className="text-center">GBP</h2>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="row">
                                <h5 className="text-center">Receive</h5>
                            </div>
                            <div className="row">
                                <h2 className="text-center">BTC</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <Field
                                name="gbp"
                                component={this.renderField}
                                normalize={this.normalizeGBP}
                                validate={[this.limitGBP]}
                                placeholder={this.state.currencySymbol + ' ' + this.state.placeholder}
                            />
                        </div>
                        <div className="col-md-6">
                            <Field
                                name="btc"
                                component={this.renderField}
                                normalize={this.normalizeBTC}
                                //placeholder={this.convertToBTC(this.state.placeholder).toFixed(8)}
                                placeholder={ this.state.placeholderBTC }
                            />
                        </div>
                    </div>
                    <div className="row buffer-top">
                        <div className="col-md-12">
                            <Field
                                name="wallet"
                                touched = {this.props.validWallet}
                                component={this.renderField}
                                valid = {this.props.validWallet}
                                placeholder={'Enter wallet address...'}
                            />
                        </div>
                    </div>
                    <div className="row buffer-top">
                        <div className="col-md-6">
                            <h6 className="text-primary">Rate: {this.state.currencySymbol + this.state.rate.toFixed(2) + '/BTC'}</h6>
                        </div>
                        <div className="col-md-6">
                            <h6 className="text-right text-danger">Limit: {this.state.currencySymbol + this.state.limit}</h6>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            { this.renderButton() }
                        </div>
                    </div>
                </div>
            </form>
        )
    }

    render() {
        if(this.state.screen === 'first') {
            return(
                <div>
                    {this.fistScreen()}
                </div>
            )
        }
        else if(this.state.screen === 'second') {
            return (  
                <ReviewForm 
                    btc={this.props.btc}
                    refreshTime = { this.state.reviewRefreshTime }
                    gbp={this.props.gbp} 
                    wallet={this.props.wallet} 
                    rate={this.state.rate} 
                    accounts={this.props.bank.accounts} 
                    callback={this.back}
                />
            )
        }
        else {
            return (
                <CreateOrderForm 
                    btc={this.props.btc}
                    gbp={this.props.gbp}
                    wallet={this.props.wallet}
                    rate={this.state.rate}
                    callback={this.back}
                />
            )
        }
    }
}


function validate(values, props) {
    const errors = {};

    // validate inputs from 'values'
    if(!values.gbp) {
      errors.gbp = "Enter gbp amount";
    }

    if (!values.btc) {
      errors.btc = "Enter btc amount";
    }
    
    if(!values.wallet) {
      errors.wallet = "Enter a valid wallet!";
    }
  
    return errors;
}

const mapStateToProps = (state) => {
    const selector = formValueSelector('CalcForm')
    let gbp = '"' + selector(state, 'gbp')
    gbp = Number.parseFloat(gbp.split(' ')[1])
    const btc = Number.parseFloat(selector(state, 'btc'))
    const wallet = selector(state, 'wallet')
    let validWallet = walletValidator.validate(wallet,'bitcoin')
    return { bank: state.bank, quote: state.quote, limit: state.limit, wallet, validWallet, gbp, btc };
 };

 
export default reduxForm({
    form: 'CalcForm',
    validate
})(connect(mapStateToProps,{ fetchQuote, fetchLimit, fetchConsts, fetchAccounts })(Calculator));
  